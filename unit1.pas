unit Unit1;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, StdCtrls, ExtCtrls,
  Menus, Windows;

type

  { TForm1 }

  TForm1 = class(TForm)
    Label1: TLabel;
    MenuItem1: TMenuItem;
    PopupMenu1: TPopupMenu;
    TrayIcon1: TTrayIcon;
    procedure FormCreate(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: char);
    procedure Label1Click(Sender: TObject);
    procedure MenuItem1Click(Sender: TObject);

    procedure UpdateCaption();
    function GetCurrentDisplaySettings() : DeviceMode;
    procedure FlipDisplaySettings();

  private

  public

  end;

var
  Form1: TForm1;

function flipDeviceModeResolution(DeviceMode: TDeviceMode) : TDeviceMode;

implementation

{$R *.lfm}

{ TForm1 }

function flipDeviceModeResolution(DeviceMode: TDeviceMode) : TDeviceMode;
begin
  if (DeviceMode.dmDisplayFrequency = 60) then begin
    DeviceMode.dmDisplayFrequency := 59;
  end
  else begin
    DeviceMode.dmDisplayFrequency := 60;
  end;
  Result := DeviceMode;
end;

function TForm1.GetCurrentDisplaySettings() : DeviceMode;
var
  DeviceMode: TDeviceMode;
begin
  EnumDisplaySettings(nil, $FFFFFFFE, DeviceMode);

  (*
  // If you wanted to see the details, then:

  // Add to var section:
    ScreenWidth : Integer;
    ScreenHeight : Integer;
    ScreenBitsPerPel : Integer;
    ScreenRefreshRate : Integer;

  // Add to code's body:
    ScreenWidth := DeviceMode.dmPelsWidth;
    ScreenHeight := DeviceMode.dmPelsHeight;
    ScreenBitsPerPel := DeviceMode.dmBitsPerPel;
    ScreenRefreshRate := DeviceMode.dmDisplayFrequency;
  *)

  Result := DeviceMode;
end;

procedure TForm1.FlipDisplaySettings();
var
  DeviceMode: TDeviceMode;
begin
  DeviceMode := Form1.GetCurrentDisplaySettings();
  DeviceMode.dmFields:=DM_PELSWIDTH or DM_PELSHEIGHT or DM_DISPLAYFREQUENCY or DM_BITSPERPEL;

  DeviceMode := flipDeviceModeResolution(DeviceMode);

  ChangeDisplaySettings(DeviceMode, CDS_UPDATEREGISTRY);
end;

procedure TForm1.Label1Click(Sender: TObject);
begin
  Form1.FlipDisplaySettings();
  Form1.UpdateCaption();
end;

procedure TForm1.MenuItem1Click(Sender: TObject);
begin
  Form1.Close();
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
  Form1.FlipDisplaySettings();
  Form1.UpdateCaption();
end;

procedure TForm1.FormKeyPress(Sender: TObject; var Key: char);
begin
  if (ord(Key) = VK_ESCAPE) then begin;
    Form1.Close();
  end;
  if (ord(Key) = VK_RETURN) then begin;
    Form1.Label1Click(Sender);
  end;
end;

procedure TForm1.UpdateCaption();
var
  CurrentDisplaySettings: TDeviceMode;
begin;
  CurrentDisplaySettings := Form1.GetCurrentDisplaySettings();

  Form1.Caption := 'Display fixer [by Jacek] :: ' + IntToStr(CurrentDisplaySettings.dmPelsWidth) + 'x' + IntToStr(CurrentDisplaySettings.dmPelsHeight) + ' @ ' + IntToStr(CurrentDisplaySettings.dmDisplayFrequency) + 'Hz';
end;

end.

