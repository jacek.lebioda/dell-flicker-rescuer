# dell-flicker-rescuer

A small application to help you with the flickering of Dell U2717D screens on Dell Laptops when they are connected via Dell docking stations (notice the irony in brand incompatibility)

# Information

The project is written in Delphi with the help of [Lazarus (Free Pascal IDE)](https://www.lazarus-ide.org).

# Downloads

Go to project's [Releases](https://git-r3lab.uni.lu/jacek.lebioda/dell-flicker-rescuer/-/releases)

# Usage

Just run the program, and proceed to click the form until the problem is fixed.

# Notes

Installing [this very driver](https://www.dell.com/support/home/us/en/04/drivers/driversdetails?driverid=fv9kp) can help you, too.